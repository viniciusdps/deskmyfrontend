import React, { useState } from 'react';
import {Link} from 'react-router-dom'

import { Layout, Menu, Avatar, Select, Input, Button } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, StarFilled, CheckCircleFilled } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { Option } = Select;
const { Search } = Input
export default function MainForm(Mcontent) { 
    return ( 
        <Layout style={{ height: '100%'}}>
        <Header className="header" style={{ height: '48px'}}> 
        <div className = "topbar">
            <h3>###</h3>
            <h3>###</h3>
            <div className = "topbar--buttons">
                <h3>###</h3>
                <h3>###</h3>
            </div>
        </div>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
          </Menu>
        </Header>
        <Layout>
          <Sider width={225} className="site-layout-background">  
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              className = "Side-div"
              style={{ height: '100%', borderRight: 0 }}
            > 
              <div className = "sidebar-div">
               <Link to = '/perfil' className = "sidebar-div">  <Avatar size={64} icon={<UserOutlined />}/>  </Link>
                <div className = "sidebar-userinfo">
                  <div className = "sidebar-userinfo__tittles">
                    <h2 style = {{margin:"30px"}}>Nome</h2>
                    <h1> #######</h1>
                   </div> 
                   <Select defaultValue="disponível"style={{ width: 200 }}> 
                      <Option value="ocupado">Ocupado</Option>
                      <Option value="disponível">Disponível</Option>
                   </Select>
                   <div className = "Media">
                    <Button type="primary" icon = {<CheckCircleFilled /> } className = "Media--button"> </Button>
                    <Button type="primary" icon = {<CheckCircleFilled /> } className = "Media--button"> </Button>
                    <Button type="primary" icon = {<CheckCircleFilled /> } className = "Media--button"> </Button>
                   </div>
                </div>                      
              </div>
              <div className = "Side-search">
                <Search 
                    placeholder="input search text"
                    onSearch={value => console.log(value)}
                    style={{ width: 200 }}
                />  
              </div>                   
                <SubMenu key="sub1" icon={<StarFilled />} title="Favoritos" className = "SubTittle">
                    <Menu.Item key="1">option1</Menu.Item>
                    <Menu.Item key="2">option2</Menu.Item>
                    <Menu.Item key="3">option3</Menu.Item>
                </SubMenu>              
              <SubMenu key="sub2" icon={<LaptopOutlined />} title="Marketing" className = "SubTittle">
                  <Menu.Item key="1">option1</Menu.Item>
                  <Menu.Item key="2">option2</Menu.Item>
                  <Menu.Item key="3">option3</Menu.Item>
              </SubMenu>  
              <SubMenu key="sub3" icon={<NotificationOutlined />} title="Sala de Criação" className = "SubTittle"></SubMenu>
              <SubMenu key="sub4" icon={<NotificationOutlined />} title="Sala Relax " className = "SubTittle"></SubMenu>
              <SubMenu key="sub4" icon={<NotificationOutlined />} title="Não disponíveis " className = "SubTittle"></SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }} className = "Main__content">
            {Mcontent ? Mcontent.children: <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
            </Content> }
          </Layout>
        </Layout>
      </Layout>
    )
}
