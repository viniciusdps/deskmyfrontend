import React from 'react'
import { Route, Switch} from 'react-router-dom';
import { Layout, Menu, Breadcrumb } from 'antd';
import Login from '../pages/login'
import Home from '../pages/Home/Home'
import Perfil from '../pages/Perfil/Perfil'
import EditP from '../pages/EdIitP/EditP'
const Routes = () => (
      <Switch>
          <Route  exact path = "/login" component ={Login}/>
          <Route exact path = "/"/>
          <Route exact path = "/home" component = {Home} />
          <Route path = "/perfil" component = {Perfil} />
          <Route path = "/edit_perfil" component = {EditP } />
      </Switch>
)
export default Routes