import React from 'react'

import { Form, Input, Card, Radio } from 'antd';
import MainForm from '../../Forms/MainForm'
import './Edit.scss'

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
      
      const validateMessages = {
        required: '${label} is required!',
        types: {
          email: '${label} is not validate email!',
          number: '${label} is not a validate number!',
        },
        number: {
          range: '${label} must be between ${min} and ${max}',
        },
      };
    export default function EditP() { 
            const onFinish = values => {
              console.log(values);
            };     
        return ( 
            <MainForm >
                <Card title="Editar Perfil" extra={<a href="#">Salvar</a>} 
                className = "edit-card "
                >
                    <Form layout = "inline " > 
                        <Form layout = "vertical"> 
                            <Form.Item label = "SOBRENOME" name = "SOBRENOME"> 
                                <Input type = "password"   className = "Login__Input" style = {{maxWidth:'400px',width:'400px'}} /> 
                            </Form.Item>
                        </Form>
                        <Form layout = "vertical"> 
                            <Form.Item label = "SOBRENOME" name = "SOBRENOME"> 
                                <Input type = "password" className = "Login__Input" style = {{maxWidth:'400px',width:'400px'}}/> 
                            </Form.Item>
                        </Form>
                    </Form>   
                    <Form layout = "vertical"> 
                            <Form.Item label = "EMAIL" name = "EMAIL"> 
                                <Input type = "password" className = "Login__Input" /> 
                            </Form.Item>
                    </Form>  
                    <Form  name="radio-group" label="Radio.Group" layout = "vertical">
                            <Form.Item  name="radio-group" label="Como vocẽ prefere ser chamado(a) ?" layout = "vertical">
                                <Radio.Group>
                                    <Radio value="a">no feminino</Radio>
                                    <Radio value="b">no masculino</Radio>
                                </Radio.Group>
                            </Form.Item>     
                    </Form>                                               
                </Card>
                <Card size="small" title="Configuraçoes de segurança" extra={<a href="#">Salvar</a>}
                className = "edit-card ">
                    <Form layout = "vertical"> 
                            <Form.Item label = "NOVA SENHA" name = "NOVA_SENHA"> 
                                <Input type = "password" className = "Login__Input" /> 
                            </Form.Item>
                    </Form>  
                    <Form layout = "vertical"> 
                            <Form.Item label = "CONFIRMAÇÃO DA NOVA SENHA" name = "CONFIRMAÇAO_SENHA"> 
                                <Input type = "password" className = "Login__Input" /> 
                            </Form.Item>
                    </Form>  
                </Card>
            </MainForm>
        )
    }
    