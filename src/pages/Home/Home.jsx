import React, { useState } from 'react';

import MainForm from '../../Forms/MainForm'
import './Home.scss'

import { Layout, Menu, Avatar, Select, Radio, Input, Button} from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, StarFilled, CheckCircleFilled } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { Option } = Select;
const { Search } = Input
export default function Home() { 
    
    return ( 
        <MainForm/>
    )
}
