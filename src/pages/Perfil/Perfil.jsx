import React from 'react';
import MainForm from '../../Forms/MainForm'
import './Perfil.scss'
import { Link } from 'react-router-dom'
import { Card, Avatar } from 'antd';

import { UserOutlined, LaptopOutlined, NotificationOutlined, StarFilled, CheckCircleFilled } from '@ant-design/icons';

export default function Perfil() { 
        const onFinish = values => {
          console.log(values);
        };     
    return ( 
        <MainForm >
            <Card title="" extra={<Link to ="/edit_perfil">Editar</Link>} 
            className = "Profile-card">
                <div className = "Profile-card__avatar">
                    <Avatar size={100} icon={<UserOutlined />}/>    
                </div>
                <div className = "Profile-card__infos">
                    <h1>Nome e Sobrenome </h1>
                    <p>Email:</p>
                    <p>Empresa:</p>
                    <p>Função:</p>
                    <p>Sobre:Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                </div>
            </Card>
        </MainForm>
    )
}


{/* <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}
className = "nest-messages">
<Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
<Input />
</Form.Item>
<Form.Item name={['user', 'email']} label="Email" rules={[{ type: 'email' }]}>
<Input />
</Form.Item>
<Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
<InputNumber />
</Form.Item>
<Form.Item name={['user', 'Role']} label="Role">
<Input />
</Form.Item>
<Form.Item name={['user', 'Company']} label="Company">
<Input />
</Form.Item>            
<Form.Item name={['user', 'introduction']} label="Introduction">
<Input.TextArea />
</Form.Item>
<Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
<Button type="primary" htmlType="submit">
    Submit
</Button>
</Form.Item>
</Form>     */}