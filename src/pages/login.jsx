import React from 'react';
import { Link } from 'react-router-dom'
import { Input, Form, Button} from 'antd';



import './login.scss'
export default function Login() { 
   return ( 
       <>    
       <div className = "main">
            <div className = "help"></div>
            <div className = "Center">
                <span> aaaaaaaaaaaaaaaaaa</span>
                <div className = "Login-Container">
                    <div className = "Login"> 
                        <h1 className = "Login__tittle"> Login</h1>
                        <Form layout = "vertical " > 
                            <Form.Item label = "EMAIL" name = "email"> 
                                <Input placeholder = "Digite seu email" size = "large" className = "Login__Input"/> 
                            </Form.Item>
                            <Form.Item label = "SENHA" name = "senha"> 
                                <Input type = "password" placeholder = "Digite sua senha" size = "large" className = "Login__Input"/> 
                            </Form.Item>
                        </Form>      
                        <Link to = '/home'  > <Button type = "primary" className = "Login__button" > Login     </Button> </Link>
                        <Link to = '/'  > <Button type = "primary" className = "Login__button" > Registrar </Button> </Link>
                    </div>
                </div>
            </div>
         </div>
       </>
    )
}
